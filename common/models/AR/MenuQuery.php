<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.02.2017
 * Time: 13:52
 */

namespace common\models\AR;


use creocoder\nestedsets\NestedSetsQueryBehavior;
use yii\db\ActiveQuery;

class MenuQuery extends ActiveQuery {

    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
}