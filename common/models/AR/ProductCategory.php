<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.02.2017
 * Time: 12:23
 */

namespace common\models\AR;


use backend\traits\NestedSets;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\caching\DbDependency;
use yii\db\ActiveRecord;
use yii\db\Query;

class ProductCategory extends ActiveRecord {
    use NestedSets;

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['creator_id'],
                ],
                'value'      => (\Yii::$app->user->identity) ? \Yii::$app->user->identity->getId() : 0,
            ],
        ];
    }

}