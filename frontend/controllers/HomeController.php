<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.02.2017
 * Time: 0:09
 */

namespace frontend\controllers;


use yii\web\Controller;

class HomeController extends Controller {

    public function actionIndex(){
        return $this->render('index');
    }
}