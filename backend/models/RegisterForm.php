<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.02.2017
 * Time: 14:27
 */

namespace backend\models;


use common\models\User;
use yii\base\Model;

class RegisterForm extends Model {

    const SEX_MALE   = 1; // мужсткой пол
    const SEX_FEMALE = 2; // женский пол

    public $id;
    public $username;
    public $first_name;
    public $last_name;
    public $sex;
    public $about_me;
    public $birthday;
    public $country;
    public $city;
    public $email;

    public function rules() {
        return [
            ['id', 'integer', 'min' => 0],
            [['first_name', 'last_name', 'username', 'about_me'], 'trim'],
            [['first_name', 'last_name', 'username', 'about_me'], 'string'],
            [['username', 'email'], 'required'],
            ['email','email'],
            ['sex', 'integer'],
            ['sex', 'in', 'range' => [self::SEX_MALE, self::SEX_FEMALE]],
            ['birthday', 'date'],
        ];
    }


    public function attributeLabels() {
        return [
            'first_name' => \Yii::t('app', 'First name'),
            'last_name'  => \Yii::t('app', 'Last name'),
            'sex'        => \Yii::t('app', 'Sex'),
            'about_me'   => \Yii::t('app', 'About me'),
        ];
    }

    public function getSexTags() {
        return [self::SEX_MALE => \Yii::t('app', 'Male'), self::SEX_FEMALE => \Yii::t('app', 'Female')];
    }

    public function save($validate = true) {
        if ($validate && !$this->validate()) {
            return false;
        }

        $id = intval($this->id);
        if ($id > 0) {
            $u = User::findOne(['id'=>$id]);
        } else {
            $u = new User();
            $u->setPassword(\Yii::$app->security->generateRandomString(12));
        }
        if (is_null($u)) return false;

        $u->attributes = $this->attributes;
//        print_r($u);
//$u->save();print_r($u->errors);exit;
        return $u->save();
    }
}