<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.02.2017
 * Time: 13:11
 */

namespace backend\assets;


use yii\web\AssetBundle;

class AngularAsset extends AssetBundle {
    public $js = [
        '//ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js',
        '//ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular-route.min.js',
        //        '//code.angularjs.org/1.6.2/angular-route.min.js',
    ];

    public $depends = [
        'backend\assets\AppAsset',
    ];
}