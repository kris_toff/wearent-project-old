<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 19.02.2017
 * Time: 23:39
 */

namespace backend\controllers;


use yii\web\Controller;

class PanelController extends Controller {
    public function actionIndex() {
        return $this->render('index');
    }
}