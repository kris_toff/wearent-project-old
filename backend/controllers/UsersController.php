<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.02.2017
 * Time: 0:29
 */

namespace backend\controllers;


use backend\models\RegisterForm;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class UsersController extends Controller {
    public function actionAll() {
        $users = new ActiveDataProvider([
            'query'      => User::find()->select(['id', 'username','email'])->asArray(),
            'sort'       => false,
            'pagination' => [
                'pageSize' => 40,
            ],
            'key' => 'id',
        ]);

        return $this->render('all', [
            'dataProvider' => $users,
        ]);
    }

    public function actionRoles() {
//        $roles = (new Query())->from('{{%auth_item}}')->where(['type' => 1])->all();

        return $this->render('roles', [
//            'roles' => ArrayHelper::map($roles, 'name', 'name'),
        ]);
    }

    public function actionUpdateUser($id = null) {
        $model = new RegisterForm();
        if (!is_null($id)) {
            $id = intval($id);
            $user = User::findOne($id);
            if (is_null($user)) throw new BadRequestHttpException("Invalid user id $id");

            $model->attributes = $user->attributes;
        }

        $request = \Yii::$app->request;
        if ($request->isPost && $model->load($request->post())) {
            if ($model->validate()) {
                $model->save(false);

                return $this->redirect(['all']);
            }
        }

        return $this->render('update-user', [
            'model' => $model,
        ]);
    }
}