<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.02.2017
 * Time: 22:16
 */

namespace backend\controllers;


use common\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class RewardController extends Controller {

    public function actionIndex(){
        $query = User::find();

        $bestSeller = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' =>20,
            ],
            'sort' => [

            ],
        ]);

        $activeParticipant = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [],
        ]);

        $fromTeam = new ActiveDataProvider([
            'query' => $query->where(['reward_team'=>1]),
            'pagination'=>[
                'pageSize' => 20,
            ],
            'sort'=>[],
        ]);

        return $this->render('index',[
            'bestSeller'=>$bestSeller,
            'activeParticipant' =>$activeParticipant,
            'fromTeam'=>$fromTeam,
        ]);
    }
}