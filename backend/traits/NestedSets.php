<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.02.2017
 * Time: 18:13
 */

namespace backend\traits;

use yii\caching\DbDependency;
use yii\db\Query;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

trait NestedSets {

    public static function hierarhySets() {
        $cache = \Yii::$app->cache;

        return $cache->getOrSet(['object' => 'nestedsets.hierarhy'], function($cache) {
            $models = static::find()->asArray()->indexBy('id')->all();
            $root   = array_filter($models, function($value) {
                return !ArrayHelper::getValue($value, 'parent_id', 0);
            });

            $nest    = array_diff_key($models, $root);
            $nestMap = ArrayHelper::map($nest, 'id', 'id', 'parent_id');

            return self::makeTree($root, $nest, $nestMap);
        }, 86400, new DbDependency([
            'sql' => (new Query())->select('MAX([[updated_at]])')->from(static::tableName())->createCommand()->sql,
        ]));
    }

    protected static function makeTree($source, $sets, $group) {
        foreach ($source as $k => $v) {
            if (ArrayHelper::keyExists($k, $group, false)) {
                $source[ $k ]['items'] = self::makeTree(array_intersect_key($sets, $group[ $k ]), $sets, $group);
            }
        }

        return array_values($source);
    }
}