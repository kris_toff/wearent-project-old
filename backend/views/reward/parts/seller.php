<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.02.2017
 * Time: 23:24
 */

use yii\grid\GridView;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'username',
    ]
]); ?>

