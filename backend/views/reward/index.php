<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.02.2017
 * Time: 22:17
 */

use kartik\tabs\TabsX;
use yii\bootstrap\Tabs;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $bestSeller */
/** @var \yii\data\ActiveDataProvider $activeParticipant */
/** @var \yii\data\ActiveDataProvider $fromTeam */
?>

<h2>Награды</h2>

<?= Tabs::widget([
    'items' => [
        [
            'label'   => 'Активный участник сообщества',
            'content' => 'wrwerwer',
        ],
        [
            'label'   => 'Лучший продавец',
            'content' => $this->render('parts/seller', ['dataProvider' => $bestSeller]),
        ],
        [
            'label'   => 'Награда от команды',
            'content' => 'wewer',
        ],
    ],
]); ?>


