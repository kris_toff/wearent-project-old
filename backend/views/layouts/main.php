<?php

use yii\widgets\Menu;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $content string */

\backend\assets\AngularAsset::register($this);
?>

<?php $this->beginContent('@app/views/layouts/basic.php'); ?>

<div class="row">
    <div class="col-sm-3" id="backend-sidebar">
        <?= Menu::widget([
            'items' => [
                [
                    'label' => 'Пользователи',
                    'items' => [
                        ['label' => 'Список', 'url' => ['/users/all']],
                        ['label' => 'Доступы', 'url' => ['/users/roles']],
                    ],
                ],
                [
                    'label' => 'Награды',
                    'url'   => ['/reward/index'],
                ],
                [
                    'label' => 'Купоны',
                    'url'   => ['/coupon/index'],
                ],
                [
                    'label' => 'Категории/Товары',
                    'url' => ['/product/index'],
                ],
            ],
        ]); ?>
    </div>
    <div class="col-sm-9">
        <!--        --><?php //Pjax::begin([
        //            'id'           => 'content-backend-pjax',
        //            'linkSelector' => '#backend-sidebar a',
        //            'timeout'      => 3000,
        //        ]); ?>
        <?= $content; ?>
        <!--        --><?php //Pjax::end(); ?>
    </div>
</div>

<?php $this->endContent(); ?>
