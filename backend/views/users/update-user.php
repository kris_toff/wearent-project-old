<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.02.2017
 * Time: 14:33
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;

/** @var \yii\web\View $this */
/** @var \backend\models\RegisterForm $model */

?>

<?= Html::a('back', ['/users/all']); ?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'username')->textInput(); ?>
<?= $form->field($model, 'first_name')->textInput(); ?>
<?= $form->field($model, 'last_name')->textInput(); ?>

<?= $form->field($model, 'sex')->dropDownList($model->sexTags, ['prompt' => \Yii::t('app', 'Choose') . '...']); ?>
<?= $form->field($model, 'about_me')->textarea(['rows' => 4]); ?>

<?= $form->field($model, 'birthday')->widget(DateRangePicker::className()); ?>

<?= $form->field($model, 'email')->textInput(); ?>
<?= Html::submitButton('Save'); ?>

<?php ActiveForm::end(); ?>


