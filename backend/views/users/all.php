<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.02.2017
 * Time: 0:30
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('New user', ['/users/update-user'], ['class' => 'btn btn-primary']); ?>
    </div>
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => [
                'id',
                'username',
                'email:text',
                [
                    'class'      => \yii\grid\ActionColumn::className(),
                    'template' => '{update}',
                    'urlCreator' => function($action, $model, $key, $index) {
                                    return Url::to(["$action-user", 'id' => $key]);
                    },
                ],
            ],
        ]); ?>
    </div>
</div>

