<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.02.2017
 * Time: 17:54
 */

namespace backend\wizards\treehouse;


use yii\base\Exception;
use yii\base\Widget;
use yii\db\ActiveRecord;

/**
 * Class TreeHouse
 *
 * @property ActiveRecord $modelClass
 *
 * @package backend\wizards\treehouse
 */
class TreeHouse extends Widget {
    public $modelClass;

    protected $_model;

    public function init() {
        parent::init();

        try {
            $this->_model = call_user_func([$this->modelClass, 'hierarhySets']);
        } catch (Exception $e) {
            $this->_model = [];
        }
    }


    public function run() {
        print 'model<pre>';
        print_r($this->_model);

        return gettype($this->_model);
    }


}