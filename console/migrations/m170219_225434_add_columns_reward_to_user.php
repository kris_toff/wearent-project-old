<?php

use yii\db\Migration;

class m170219_225434_add_columns_reward_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'reward_best_sellet', $this->boolean()->defaultValue(0));
        $this->addColumn('{{%user}}', 'reward_active_participant', $this->boolean()->defaultValue(0));
        $this->addColumn('{{%user}}', 'reward_team', $this->boolean()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'reward_best_sellet');
        $this->dropColumn('{{%user}}', 'reward_active_participant');
        $this->dropColumn('{{%user}}', 'reward_team');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
