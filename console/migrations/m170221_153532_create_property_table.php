<?php

use yii\db\Migration;

/**
 * Handles the creation of table `property`.
 */
class m170221_153532_create_property_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%property}}', [
            'id'   => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
            'type' => $this->smallInteger()->notNull()->comment('Тип поля'),

            'creator_id' => $this->integer()->unsigned()->notNull(),
            'created_at' => $this->integer()->unsigned(),
            'updated_at' => $this->integer()->unsigned(),
        ], $tableOptions);

        $this->createIndex('idx_property_creator', '{{%property}}', 'creator_id', false);
        $this->addForeignKey('fk_property_creator', '{{%property}}', 'creator_id', '{{%user}}', 'id', 'RESTRICT',
            'CASCADE');

        $this->createTable('{{%property_assign}}', [
            'property_id' => $this->integer()->unsigned()->notNull(),
            'category_id' => $this->integer()->unsigned()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_property_assign_property_category', '{{%property_assign}}',
            ['property_id', 'category_id'], true);
        $this->addForeignKey('fk_property_assign_property', '{{%property_assign}}', 'property_id', '{{%property}}',
            'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_property_assign_category', '{{%property_assign}}', 'category_id',
            '{{%product_category}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('{{%property_assign}}');
        $this->dropTable('{{%property}}');
    }
}
