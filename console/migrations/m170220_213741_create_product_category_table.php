<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_category`.
 */
class m170220_213741_create_product_category_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product_category}}', [
            'id'           => $this->primaryKey()->unsigned(),
            'parent_id'    => $this->integer()->unsigned()->defaultValue(null)->comment('Родительская категория'),
            //            'lft'           => $this->integer()->notNull(),
            //            'rgt'           => $this->integer()->notNull(),
            //            'lvl'           => $this->smallInteger(5)->notNull(),
            'name'         => $this->string(60)->notNull()->comment('Название категории'),
            //            'icon'          => $this->string(),
            //            'icon_type'     => $this->smallInteger(1)->notNull()->defaultValue(1),
            'photo_url'    => $this->string()->comment('Ссылка на полноценное изображение'),
            'icon_url'     => $this->string()->comment('Ссылка на иконку категории'),
            'real_estate'  => $this->boolean()
                ->defaultValue(0)
                ->comment('Флаг "недвижимость" исключает доставку товара'),
            'real_popular' => $this->boolean()->defaultValue(0)->comment('Флаг "популярное" влияет на сортировку'),
            'active'       => $this->boolean()->defaultValue(1),
            //            'selected'      => $this->boolean()->defaultValue(0),
            //            'disabled'      => $this->boolean()->defaultValue(0),
            //            'readonly'      => $this->boolean()->defaultValue(0),
            //            'visible'       => $this->boolean()->defaultValue(1),
            //            'collapsed'     => $this->boolean()->defaultValue(0),
            //            'movable_u'     => $this->boolean()->defaultValue(1),
            //            'movable_d'     => $this->boolean()->defaultValue(1),
            //            'movable_l'     => $this->boolean()->defaultValue(1),
            //            'movable_r'     => $this->boolean()->defaultValue(1),
            //            'removable'     => $this->boolean()->defaultValue(1),
            //            'removable_all' => $this->boolean()->defaultValue(0),
            'creator_id'   => $this->integer()->notNull()->unsigned()->comment('ID создателя категории'),
            'created_at'   => $this->integer()->unsigned(),
            'updated_at'   => $this->integer()->unsigned(),
        ], $tableOptions);

        $this->createIndex('idx_product_category_parent', '{{%product_category}}', 'parent_id', false);
//        $this->createIndex('idx_product_category_lft', '{{%product_category}}', 'lft', false);
//        $this->createIndex('idx_product_category_rgt', '{{%product_category}}', 'rgt', false);
//        $this->createIndex('idx_product_category_lvl', '{{%product_category}}', 'lvl', false);
        $this->createIndex('idx_product_category_active', '{{%product_category}}', 'active', false);
        $this->createIndex('idx_product_category_creator', '{{%product_category}}', 'creator_id', false);
        $this->addForeignKey('fk_product_category_creator', '{{%product_category}}', 'creator_id', '{{%user}}', 'id',
            'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropForeignKey('fk_product_category_creator', '{{%product_category}}');
        $this->dropTable('{{%product_category}}');
    }
}
