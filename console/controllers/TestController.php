<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.02.2017
 * Time: 0:51
 */

namespace console\controllers;


use yii\console\Controller;

class TestController extends Controller {

    public function actionIndex(){
        $auth = \Yii::$app->authManager;

        $this->stdout('OK');
    }
}